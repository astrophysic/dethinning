#ifndef _DETHINNING_HPP_
#define _DETHINNING_HPP_

#include "CorsikaParticle.hpp"

void Dethinnig(CorsikaParticle* primary, CorsikaParticle* current, ParticleArray* output);

#endif /* _DETHINNING_HPP_ */