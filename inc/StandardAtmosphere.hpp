#ifndef _STANDARD_ATMOSPHERE_H_
#define _STANDARD_ATMOSPHERE_H_

#include <cmath>

double HeightToSlant(double h);
double SlantToHeight(double x);

#endif /* _STANDARD_ATMOSPHERE_H_ */
