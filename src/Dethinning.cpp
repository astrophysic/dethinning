#include "Dethinning.hpp"
#include "StandardAtmosphere.hpp"

#include <random>

void Dethinnig (CorsikaParticle* primary, CorsikaParticle* current, ParticleArray* output)
{
    _4Vector position = current->GetPosition();
    _4Vector p_position = primary->GetPosition();
    _4Vector momentum = current->GetMomentum();

    double theta = current->GetTheta();
    double phi = current->GetPhi();

    double dist_max = sqrt (
                        pow(position[1]-p_position[1],2) +
                        pow(position[2]-p_position[2],2) +
                        pow(position[3]-p_position[3],2) 
                      );
    dist_max = dist_max - (   
                            SlantToHeight(0)/100
                            - SlantToHeight( 30*current->GetGeneration()*sin(theta) )/100
                          );

    // Calculating the number of particles output                          
    double thinning = current->GetThinning();
    double wmax = (int) thinning;
    if (rand()/RAND_MAX > thinning - (int)thinning)
    {
        wmax++;
    }
    //Adding first particle
    // particle->SetThinning(1);
    output->push_back( new CorsikaParticle(position, momentum,current->GetType(), 1, current->GetGeneration()));



    //Normal distribution generator
    double rho = sqrt(position[1]*position[1]+position[2]*position[2]);
    double sig = (M_PI/180)*(rho/1000);
    std::default_random_engine generator;
    std::normal_distribution<double> gauss(0,sig);

    for (size_t i = 1; i < wmax; i++ )
    {
        double alphaX = gauss(generator);
        double alphaY = gauss(generator);

        double gamma, delta;
        double A, B, C;
        double F, G;

        double H, X;  //Altura e Profundidade
        double DT; //Diferença de tempo da nova trajetória
        double Prob;

        if (alphaX > 0) 
        {
            gamma = M_PI/2-alphaX-theta;
            delta=M_PI-alphaX-gamma;

            double sinG = sin(gamma);
            double sinAX = sin(alphaX);
            double sinD = sin(delta);

            A = dist_max * sinD/sinG;
            B = dist_max * sinAX/sinG;

            double tgAY = tan(alphaY);
            double cosAY = cos(alphaY);

            F = A * tgAY;
            G = A / cosAY;

            H = (G-dist_max);
            X = (HeightToSlant(0)-HeightToSlant(100*H*sinG))/sinG;
            DT = H/(3*pow(10,-1)); // dt = c * H  (ns)
        }
        else
        {
            alphaX=-1*alphaX;
            gamma = M_PI/2-theta;
            delta=M_PI-(alphaX)-gamma;

            double sinG = sin(gamma);
            double sinAX = sin(alphaX);
            double sinD = sin(delta);

            C = dist_max * sinG/sinD;
            B = -1 * dist_max * sinAX/sinD;

            double tgAY = tan(alphaY);
            double CosAY = cos(alphaY);

            F = C * tgAY;
            G = C / CosAY;

            H = (G-dist_max);
            if (H < 0 )
            {
                X = -1;
            }
            else
            {
                X = (HeightToSlant(0)-HeightToSlant(100*H*sinG))/sinG;   // problema m e cm
            }
            DT = H/(3*pow(10,-1)); // dt = c * H  (ns)   
        }
        double DX = B;
        double DY = F;
        // double EN = Generator.Gaus(Energy,0.1*Energy);
        std::normal_distribution<double> energy_gauss(momentum[0],0.1*momentum[0]);

        //Testando a probabilidade da partícula existir após percorrer outra trajetória
        if (X > 0){
        Prob = exp(-X/50);
        } else {
        Prob =1;
        }

        if (rand()/RAND_MAX < Prob)
        {
        _4Vector _position(position[0]+DT,position[1]+DX,position[2]+DY,position[3]);

        double _energy = energy_gauss(generator);
        _4Vector _momentum(_energy,_energy*sin(alphaX+theta)*cos(alphaY+phi), _energy*sin(alphaX+theta)*sin(alphaY+phi),_energy*cos(alphaX+theta));

        output->push_back( new CorsikaParticle(_position, _momentum, current->GetType(), 1, current->GetGeneration()));
        }
    }
}