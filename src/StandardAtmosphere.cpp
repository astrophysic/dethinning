#include "StandardAtmosphere.hpp"

double SlantToHeight(double x){
   //Recive Slanth Depth X in g/cm^2 and return altitude in cm
   double h;
   double a[5]={-186.5562,-94.919,0.61289,0.0,0.01128292};
   double b[5]={1222.6562,1144.9069,1305.5948,540.1778,1.0};
   double c[5]={994186.38,878153.55,636143.04,772170.16,1000000000};
   //Standard atmosphere U.S


   if (x < 0.00128292){
      h = -1.0*(x-a[4])/b[4]*c[4];
   }
   else if (x < 3.039560){
      h = -1*c[3]*log((x-a[3])/b[3]);
   }
   else if (x < 271.70089){
      h = -1*c[2]*log((x-a[2])/b[2]);
   }
   else if (x < 631.09999){
      h = -1*c[1]*log((x-a[1])/b[1]);
   }
   else{
       h = -1*c[0]*log((x-a[0])/b[0]);
   }
   return h;

}

double HeightToSlant(double h){
  double x;
  double a[5]={-186.5562,-94.919,0.61289,0.0,0.01128292};
  double b[5]={1222.6562,1144.9069,1305.5948,540.1778,1.0};
  double c[5]={994186.38,878153.55,636143.04,772170.16,1000000000};


  if (h < 400000 ){
    x = a[0]+b[0]*exp(-h/c[0]);
  } else if (h < 1000000){
    x = a[1]+b[1]*exp(-h/c[1]);
  } else if (h < 4000000){
    x = a[2]+b[2]*exp(-h/c[2]);
  } else if (h < 10000000){
    x = a[3]+b[3]*exp(-h/c[3]);
  } else {
    x = a[4]-b[4]*h/c[4];
  }
  return x;
}
